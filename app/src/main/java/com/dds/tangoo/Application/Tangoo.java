package com.dds.tangoo.Application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.dds.tangoo.R;
import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import io.fabric.sdk.android.Fabric;

/**
 * Created by DDS on 1/1/2017.
 */

@ReportsCrashes(applicationLogFile = "Tangoo App Crushed",
        mailTo = "info@dreamdoorsoft.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class Tangoo extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Firebase.setAndroidContext(this);

        if(!FirebaseApp.getApps(this).isEmpty()){

            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
    }
}
