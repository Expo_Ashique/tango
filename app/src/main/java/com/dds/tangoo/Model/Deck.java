package com.dds.tangoo.Model;

/**
 * Created by DDS on 12/31/2016.
 */

public class Deck {
    public String title;
    public String timeStamp;

    public Deck(String title, String timeStamp) {
        this.title = title;
        this.timeStamp = timeStamp;
    }

    public Deck() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
