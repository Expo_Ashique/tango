package com.dds.tangoo.Model;

/**
 * Created by DDS on 1/1/2017.
 */

public class Items {
    public String cardA;
    public String cardB;
    public String active;
    public String allWordsNotifier;


    public Items(String cardA, String cardB, String active, String allWordsNotifier) {
        this.cardA = cardA;
        this.cardB = cardB;
        this.active = active;
        this.allWordsNotifier = allWordsNotifier;
    }

    public Items() {
    }

    public String getCardA() {
        return cardA;
    }

    public void setCardA(String cardA) {
        this.cardA = cardA;
    }

    public String getCardB() {
        return cardB;
    }

    public void setCardB(String cardB) {
        this.cardB = cardB;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAllWordsNotifier() {
        return allWordsNotifier;
    }

    public void setAllWordsNotifier(String allWordsNotifier) {
        this.allWordsNotifier = allWordsNotifier;
    }

}
