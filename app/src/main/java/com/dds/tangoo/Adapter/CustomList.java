package com.dds.tangoo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dds.tangoo.R;

import java.util.ArrayList;

/**
 * Created by DDS on 2/8/2017.
 */

public class CustomList extends ArrayAdapter<String> {
    public ArrayList<String> allWords;
    public Integer[] imageId;
    public Activity context;
    public CustomList(Activity context, ArrayList<String> allWords,Integer[] imageId) {
        super(context, R.layout.list, allWords);
        this.context = context;
        this.allWords = allWords;
        this.imageId = imageId;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list, null, true);
        TextView words = (TextView) listViewItem.findViewById(R.id.list_text);
        ImageView status = (ImageView) listViewItem.findViewById(R.id.status_img);

        words.setText(allWords.get(position));
        status.setImageResource(imageId[position]);
        return  listViewItem;
    }
}
