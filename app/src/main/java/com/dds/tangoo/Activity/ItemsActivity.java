package com.dds.tangoo.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.tangoo.Model.Items;
import com.dds.tangoo.R;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.TimeZone;

public class ItemsActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private String userID, itemId;
    Firebase childRefItem;
    private Firebase mRootRef;
    Button playBtn, addItemBtn, cancelBtn;
    String deckValueKey, deckListItem;
    EditText cardA, cardB;
    Context context = this;
    String itemKeyValue;
    ArrayList<String> itemKeyValueArrayList = new ArrayList<>();
    ListView item_list;
    ArrayList<String> itemvalueAnsList = new ArrayList<>();
    ArrayList<String> itemvalueList;
    ArrayList<String> wrongAnswerList = new ArrayList<>();
    ArrayList<String> wrongQuestionList = new ArrayList<>();
    FirebaseListAdapter<Items> firebaseListAdapter;
    TextView happyText, sadText, itemName;
    ImageView closeDialogImageView;
    Button allWordsBtn, wrongWordsBtn, notYetWordsBtn;
    MediaPlayer mp;
    private boolean isAlart = false;
    int happyCounter = 0, sadCounter = 0;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        item_list = (ListView) findViewById(R.id.item_list);
        happyText = (TextView) findViewById(R.id.happytext);
        sadText = (TextView) findViewById(R.id.sadtext);
        playBtn = (Button) findViewById(R.id.play);
        itemName = (TextView) findViewById(R.id.item_name);
        auth = FirebaseAuth.getInstance();
        itemvalueList = new ArrayList<>();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    finish();
                }
            }
        };

        wrongQuestionList.clear();
        wrongAnswerList.clear();
        Bundle bundle = getIntent().getExtras();
        deckValueKey = bundle.getString("deckValueKey");
        deckListItem = bundle.getString("deckTitle");

        Intent i = getIntent();
        wrongQuestionList = i.getStringArrayListExtra("wrongQuestions");
        wrongAnswerList = i.getStringArrayListExtra("wrongAnswers");

        itemName.setText(deckListItem);
        mp = MediaPlayer.create(this, R.raw.open);

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://tangoo2-54486.firebaseio.com/items/" +
                        auth.getCurrentUser().getUid() + "/" + deckValueKey);
        databaseReference.keepSynced(true);
        firebaseListAdapter = new FirebaseListAdapter<Items>(this, Items.class, R.layout.list, databaseReference) {
            @Override
            protected void populateView(View v, Items model, int position) {
                isAlart = true;
                alertDialog.dismiss();
                TextView textView = (TextView) v.findViewById(R.id.list_text);
                ImageView statusImg = (ImageView) v.findViewById(R.id.status_img);
                String question = model.getCardA();
                textView.setText(model.getCardA());
                itemvalueList.add(question);
                String answers = model.getCardB();
                itemvalueAnsList.add(answers);
                if (model.getActive().equals("-1")) {
                    statusImg.setImageResource(R.drawable.ic_sad_red);
                } else if (model.getActive().equals("1")) {
                    statusImg.setImageResource(R.drawable.ic_happy_blue);
                } else if (model.getActive().equals("0")) {
                    statusImg.setVisibility(v.INVISIBLE);
                }
                firebaseListAdapter.notifyDataSetChanged();
            }
        };

        item_list.setAdapter(firebaseListAdapter);
        if (!isAlart) {
            alert(deckValueKey);
        }

        Bundle point = getIntent().getExtras();
        happyCounter = point.getInt("happyCount");
        sadCounter = point.getInt("sadCount");

        happyText.setText("" + happyCounter);
        sadText.setText("" + sadCounter);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examalert();
            }
        });

        itemKeyValueArrayList.clear();
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValueKey);
        final com.firebase.client.ValueEventListener valueEventListener = mRootRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (com.firebase.client.DataSnapshot deckSnapShot : dataSnapshot.getChildren()) {
                    itemKeyValue = deckSnapShot.getKey();
                    itemKeyValueArrayList.add(itemKeyValue);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void createNewItem(String deckId) {
        final String card_A = cardA.getText().toString();
        final String card_B = cardB.getText().toString();
        final String active = "0";
        final String allWordsNotifier = "1";

        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/");
        childRefItem = mRootRef.child("items");
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("GMT+06:00"));
        userID = auth.getCurrentUser().getUid();
        itemId = mRootRef.push().getKey();

        Items items = new Items(card_A, card_B, active, allWordsNotifier);
        items.setCardA(card_A);
        items.setCardB(card_B);
        items.setActive(active);
        items.setAllWordsNotifier(allWordsNotifier);
        itemvalueList.clear();
        itemvalueAnsList.clear();
        childRefItem.child(userID).child(deckId).child(itemId).setValue(items);
    }

    @Override
    public void onClick(View v) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.add_item, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);
        cardA = (EditText) promptsView.findViewById(R.id.card_a);
        cardB = (EditText) promptsView.findViewById(R.id.card_b);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                createNewItem(deckValueKey);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(ItemsActivity.this, AddItemActivity.class);
                intent.putExtra("deckValue", deckValueKey);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void alert(final String deckValueKey) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.notice, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);
        alertDialog = alertDialogBuilder.create();

        addItemBtn = (Button) promptsView.findViewById(R.id.new_item);
        cancelBtn = (Button) promptsView.findViewById(R.id.cancel);

        addItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ItemsActivity.this, AddItemActivity.class);
                intent.putExtra("deckValue", deckValueKey);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void examalert() {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.exam, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        closeDialogImageView = (ImageView) promptsView.findViewById(R.id.close_dialog);
        allWordsBtn = (Button) promptsView.findViewById(R.id.all_word_btn);
        wrongWordsBtn = (Button) promptsView.findViewById(R.id.wrong_words_btn);
        notYetWordsBtn = (Button) promptsView.findViewById(R.id.not_yet_btn);

        allWordsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                allWordExamed(deckValueKey);
            }
        });

        wrongWordsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                wrongWordExamed(deckValueKey);
            }

        });

        notYetWordsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                notYetExamed(deckValueKey);
            }
        });

        closeDialogImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void notYetExamed(final String deckValueKey) {
        Intent intent = new Intent(ItemsActivity.this, NotYetTestActivity.class);
        intent.putExtra("deckValueListItem", deckListItem);
        intent.putExtra("deckValueKey", deckValueKey);
        startActivity(intent);
        finish();
    }

    public void wrongWordExamed(final String deckValueKey) {
        Intent intent = new Intent(ItemsActivity.this, WrongWordsTestActivity.class);
        intent.putExtra("deckValueListItem", deckListItem);
        intent.putExtra("deckValueKey", deckValueKey);
        startActivity(intent);
        finish();
    }

    public void allWordExamed(final String deckValueKey) {
        Intent intent = new Intent(ItemsActivity.this, AllWordTestActivity.class);
        intent.putExtra("deckValueKey", deckValueKey);
        intent.putExtra("deckValueListItem", deckListItem);
        startActivity(intent);
        finish();
    }
}
