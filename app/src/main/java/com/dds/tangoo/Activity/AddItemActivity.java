
package com.dds.tangoo.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dds.tangoo.Model.Items;
import com.dds.tangoo.R;
import com.firebase.client.Firebase;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddItemActivity extends AppCompatActivity {
    TextView questionText, answerText;
    Context context = this;
    Button okBtn, suggestionOkBtn, storeWordsBtn;
    EditText qusEditText, ansEditText;
    String qusEditTextValue;
    ListView suggestionList;
    ImageView close;
    FirebaseListAdapter<Items> firebaseListAdapter;
    ArrayList<String> suggestionArrayList = new ArrayList<String>();
    ArrayAdapter<String> arrayAdapter;
    Firebase childRefItem;
    private Firebase mRootRef;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private String userID, itemId, deckValueKey;
    public final static String API_LINK = "http://ik1-321-20619.vs.sakura.ne.jp/v1/api/req/getJpByEn?key=4iQsLFRiKQzo1IXZRQJvufw2cE3FIwvlG8FRyuqRfHLtfNQp43w24SErlh8mkQwv&vocab=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Bundle bundle = getIntent().getExtras();
        deckValueKey = bundle.getString("deckValue");

        storeWordsBtn = (Button) findViewById(R.id.store_btn);
        questionText = (TextView) findViewById(R.id.qus);
        answerText = (TextView) findViewById(R.id.ans);
        questionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert();
            }
        });
        answerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerAlert();
            }
        });
        storeWordsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewItems(deckValueKey);
                questionText.setText("Question");
                answerText.setText("Answer");
            }
        });

        auth = FirebaseAuth.getInstance();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    finish();
                }
            }
        };
    }

    public void alert() {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.add_words, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        qusEditText = (EditText) promptsView.findViewById(R.id.qus_edit_text);
        okBtn = (Button) promptsView.findViewById(R.id.ok_btn);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qusEditTextValue = qusEditText.getText().toString();
                questionText.setText(qusEditTextValue);
                sendRequest(qusEditTextValue);
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    public void answerAlert() {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.add_words, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        qusEditText = (EditText) promptsView.findViewById(R.id.qus_edit_text);
        okBtn = (Button) promptsView.findViewById(R.id.ok_btn);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qusEditTextValue = qusEditText.getText().toString();
                answerText.setText(qusEditTextValue);
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    public void suggestionAlert(ArrayList suggestion) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.suggestion, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptsView);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        suggestionList = (ListView) promptsView.findViewById(R.id.suggestion_list);
        close = (ImageView) promptsView.findViewById(R.id.imageView);
        suggestionOkBtn = (Button) promptsView.findViewById(R.id.ok_btn);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, suggestion);
        suggestionList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        suggestionList.setAdapter(arrayAdapter);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        suggestionOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> selectedItem = new ArrayList<String>();
                selectedItem.clear();
                SparseBooleanArray checkedItem = suggestionList.getCheckedItemPositions();
                for (int i = 0; i < checkedItem.size(); i++) {
                    // Item position in adapter
                    int position = checkedItem.keyAt(i);
                    // Add sport if it is checked i.e.) == TRUE!
                    if (checkedItem.valueAt(i)) {
//                        answerText.setText(arrayAdapter.getItem(position));
                        selectedItem.add(arrayAdapter.getItem(position));
                    }
                }
                StringBuilder builder = new StringBuilder();
                for (String items : selectedItem) {
                    builder.append(items + ",");
                }
                answerText.setText(builder.toString());
//                Toast.makeText(getApplicationContext(),""+selectedItem.toString(),Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void sendRequest(String word) {

        suggestionArrayList.clear();
        StringRequest stringRequest = new StringRequest(API_LINK + word,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("definition");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                suggestionArrayList.add(jsonArray.get(i).toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        suggestionAlert(suggestionArrayList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddItemActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

//        suggestionAlert(suggestionArrayList);
    }

    public void createNewItems(String deckId) {
        final String card_A = questionText.getText().toString();
        final String card_B = answerText.getText().toString();
        final String active = "0";
        final String allWordsNotifier = "1";
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/");
        childRefItem = mRootRef.child("items");
        userID = auth.getCurrentUser().getUid();
        itemId = mRootRef.push().getKey();

        Items items = new Items(card_A, card_B, active, allWordsNotifier);
        items.setCardA(card_A);
        items.setCardB(card_B);
        items.setActive(active);
        items.setAllWordsNotifier(allWordsNotifier);

        childRefItem.child(userID).child(deckId).child(itemId).setValue(items);
        Toast.makeText(getApplicationContext(), "Storing ...", Toast.LENGTH_LONG).show();

    }


}
