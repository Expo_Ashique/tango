package com.dds.tangoo.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionBarPolicy;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.tangoo.Model.Items;
import com.dds.tangoo.R;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AllWordTestActivity extends AppCompatActivity {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    Firebase childRefItem;
    private Firebase mRootRef;
    String itemKeyValue;
    ArrayList<String> questions;
    ArrayList<String> answers;
    ArrayList<String> desQuestions = new ArrayList<>();
    ArrayList<String> desAnswers = new ArrayList<>();
    ArrayList<String> suffleQuestions = new ArrayList<>();
    ArrayList<String> suffleAnswers = new ArrayList<>();
    ArrayList<String> questionWordArrayList = new ArrayList<>();
    ArrayList<String> answerWordArrayList = new ArrayList<>();
    ArrayList<String> itemKeyValueArrayList = new ArrayList<>();
    ArrayList<String> itemKeyList = new ArrayList<>();
    ArrayList<String> tempItemKeyList = new ArrayList<>();
    private Button retryBtn, okBtn;
    private TextView qusTxt, ansTxt;
    String deckValue, deckTitle;
    int increment = 0;
    int count = 0;
    MediaPlayer mp, mp1;
    public Context context = this;
    int k;
    ImageView closeTestFormatAlert;
    int temp = 0, counter = 0;
    int happyCount = 0, sadCount = 0;
    ArrayList<String> wrongWordQuestionArrayList = new ArrayList<String>();
    ArrayList<String> wrongWordAnswerArrayList = new ArrayList<String>();
    int shuffler;
    int min;
    int max;
    Random r = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) throws ArrayIndexOutOfBoundsException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        auth = FirebaseAuth.getInstance();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    finish();
                }
            }
        };

        retryBtn = (Button) findViewById(R.id.retry_btn);
        okBtn = (Button) findViewById(R.id.ok_btn);
        qusTxt = (TextView) findViewById(R.id.qus);
        ansTxt = (TextView) findViewById(R.id.ans);


        wrongWordQuestionArrayList.clear();
        wrongWordAnswerArrayList.clear();

        Intent i = getIntent();
        deckValue = i.getStringExtra("deckValueKey");
        deckTitle = i.getStringExtra("deckValueListItem");

        questions = new ArrayList<String>();
        answers = new ArrayList<String>();
        questions.clear();
        answers.clear();

        getDataAcendingOrder();
    }

    public void getDataAcendingOrder() {
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue);
        final ChildEventListener childEventListener = mRootRef.orderByChild("allWordsNotifier").equalTo("1").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = dataSnapshot.getValue(Map.class);
                itemKeyValueArrayList.add(dataSnapshot.getKey());
                questions.add(map.get("cardA"));
                answers.add(map.get("cardB"));
                MyClickBtn(questions, answers);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public void getDataDecendingOrder() {
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue);
        final ChildEventListener childEventListener = mRootRef.orderByChild("allWordsNotifier").equalTo("1").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = dataSnapshot.getValue(Map.class);
                itemKeyValueArrayList.add(dataSnapshot.getKey());
                questions.add(map.get("cardA"));
                answers.add(map.get("cardB"));
            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        for (int i = questions.size() - 1; i >= 0; i--) {
            desQuestions.add(questions.get(i));
        }
        for (int j = answers.size() - 1; j >= 0; j--) {
            desAnswers.add(answers.get(j));
        }
        for (int k = itemKeyValueArrayList.size() - 1; k >= 0; k--) {
            itemKeyList.add(itemKeyValueArrayList.get(k));
        }
        MyClickBtnForDesendingOrder(desQuestions, desAnswers, itemKeyList);
    }

    public void MyClickBtnForShuffle(final ArrayList<String> questions, final ArrayList<String> answers) {

        mp = MediaPlayer.create(this, R.raw.hyu);
        mp1 = MediaPlayer.create(this, R.raw.button);
        qusTxt.setText(questions.get(counter));
        ansTxt.setText("");

        ansTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "Array Size:  " + questions.size(), Toast.LENGTH_LONG).show();
                if (counter > questions.size() - 1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    finish();

                } else {
                    ansTxt.setText(answers.get(counter));

//                        Toast.makeText(getApplicationContext(), "Counter: " + counter, Toast.LENGTH_LONG).show();
                }
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                counter++;
                happyCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyValueArrayList.get(counter - 1));
                childRefItem.child("active").setValue("1");
//                            updateData(deckValue, "1");
                if (counter > questions.size() - 1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
//                    Toast.makeText(getApplicationContext(), "Happy: " + happyCount, Toast.LENGTH_LONG).show();
                }
            }

        });

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp1.start();
                counter++;
                sadCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyValueArrayList.get(counter - 1));
                childRefItem.child("active").setValue("-1");
                if (counter > questions.size() - 1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
                    wrongWordQuestionArrayList.add(questions.get(counter));
                    wrongWordAnswerArrayList.add(answers.get(counter));
//                        Toast.makeText(getApplicationContext(), "Sad: " + sadCount, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public void getDataSuffleOrder() {
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue);
        final ChildEventListener childEventListener = mRootRef.orderByChild("allWordsNotifier").equalTo("1").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = dataSnapshot.getValue(Map.class);
                itemKeyValueArrayList.add(dataSnapshot.getKey());
                questions.add(map.get("cardA"));
                answers.add(map.get("cardB"));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        int size = questions.size();
        ArrayList<Integer> dummyList = new ArrayList<Integer>();
        ArrayList<String> tempQuestions = new ArrayList<String>();
        ArrayList<String> tempAnswers = new ArrayList<String>();
        ArrayList<String> tempItemKeyValueArrayList = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            dummyList.add(i);
        }
        String temp = "";
        Collections.shuffle(dummyList);

        for (int i = 0; i < size; i++) {
            temp = questions.get(dummyList.get(i));
            tempQuestions.add(temp);
            temp = answers.get(dummyList.get(i));
            tempAnswers.add(temp);

            temp = itemKeyValueArrayList.get(dummyList.get(i));
            tempItemKeyValueArrayList.add(temp);
        }


        MyClickBtnForShuffle(tempQuestions, tempAnswers, tempItemKeyValueArrayList);

    }

    public void MyClickBtn(final ArrayList<String> questions, final ArrayList<String> answers) {

        mp = MediaPlayer.create(this, R.raw.hyu);
        mp1 = MediaPlayer.create(this, R.raw.button);
        qusTxt.setText(questions.get(counter));
        ansTxt.setText("");

        ansTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    finish();

                } else {
                    ansTxt.setText(answers.get(counter));
                }
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                counter++;
                happyCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyValueArrayList.get(counter - 1));
                childRefItem.child("active").setValue("1");
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
                }
            }

        });

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp1.start();
                counter++;
                sadCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyValueArrayList.get(counter - 1));
                childRefItem.child("active").setValue("-1");
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
                    wrongWordQuestionArrayList.add(questions.get(counter));
                    wrongWordAnswerArrayList.add(answers.get(counter));
                }

            }
        });
    }

    public void clickButtonForOptionWork(final ArrayList<String> questions, final ArrayList<String> answers, final ArrayList<String> itemKeyList) {
        mp = MediaPlayer.create(this, R.raw.hyu);
        mp1 = MediaPlayer.create(this, R.raw.button);
        qusTxt.setText(questions.get(counter));
        ansTxt.setText("");

        ansTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    finish();

                } else {
                    ansTxt.setText(answers.get(counter));
                }
            }
        });


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                counter++;
                happyCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyList.get(counter - 1));
                childRefItem.child("active").setValue("1");
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
                }
            }

        });

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp1.start();
                counter++;
                sadCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyList.get(counter - 1));
                childRefItem.child("active").setValue("-1");
                if (counter > questions.size() - 1) {
                    okBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
                    wrongWordQuestionArrayList.add(questions.get(counter));
                    wrongWordAnswerArrayList.add(answers.get(counter));
                }

            }
        });
    }

    public void okButtonWork() {
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                counter++;
                happyCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue + "/" + itemKeyList.get(counter - 1));
                childRefItem.child("active").setValue("1");
//                            updateData(deckValue, "1");
                if (counter > questions.size() - 1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(AllWordTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    intent.putStringArrayListExtra("wrongQuestions", wrongWordQuestionArrayList);
                    intent.putStringArrayListExtra("wrongAnswers", wrongWordAnswerArrayList);
                    startActivity(intent);
                    wrongWordAnswerArrayList.clear();
                    wrongWordQuestionArrayList.clear();
                    finish();
                } else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
//                    Toast.makeText(getApplicationContext(), "Happy: " + happyCount, Toast.LENGTH_LONG).show();
                }
            }

        });
    }

    public void MyClickBtnForDesendingOrder(final ArrayList<String> questions, final ArrayList<String> answers, final ArrayList<String> itemKeyList) {

        clickButtonForOptionWork(questions, answers, itemKeyList);
    }

    public void MyClickBtnForShuffle(final ArrayList<String> questions, final ArrayList<String> allAnswers, final ArrayList<String> itemKeyList) {
        clickButtonForOptionWork(questions, allAnswers, itemKeyList);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.test, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.test:
                alert();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void alert() {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.test_format, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);


        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        closeTestFormatAlert = (ImageView) promptsView.findViewById(R.id.imageView6);
        final CheckBox decandingOrder = (CheckBox) promptsView.findViewById(R.id.decend_active_btn);
        final CheckBox suffleOrder = (CheckBox) promptsView.findViewById(R.id.suffle_active_btn);

        decandingOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decandingOrder.isChecked()) {
//                    Toast.makeText(getApplicationContext(),"Decanding order",Toast.LENGTH_LONG).show();
                    getDataDecendingOrder();
                    alertDialog.dismiss();
                }
            }
        });

        suffleOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (suffleOrder.isChecked()) {
                    getDataSuffleOrder();
                    alertDialog.dismiss();
                }
            }
        });
        closeTestFormatAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
