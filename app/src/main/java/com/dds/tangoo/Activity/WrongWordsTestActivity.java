package com.dds.tangoo.Activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.tangoo.R;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Map;

public class WrongWordsTestActivity extends AppCompatActivity {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    Firebase childRefItem;
    private Firebase mRootRef;
    String itemKeyValue;
    ArrayList<String> questions;
    ArrayList<String> answers;
    ArrayList<String> itemKeyValueArrayList = new ArrayList<>();
    private Button retryBtn, okBtn;
    private TextView qusTxt, ansTxt;
    String deckValue,deckTitle;
    int increment = 0;
    int count = 0;
    MediaPlayer mp,mp1;
    public Context context = this;
    int k;
    ImageView closeTestFormatAlert;
    int temp = 0,counter = 0;
    int happyCount = 0, sadCount = 0;
    ArrayList<String> wrongWordQuestionArrayList = new ArrayList<String>();
    ArrayList<String> wrongWordAnswerArrayList = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        auth = FirebaseAuth.getInstance();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    finish();
                }
            }
        };

        retryBtn = (Button) findViewById(R.id.retry_btn);
        okBtn = (Button) findViewById(R.id.ok_btn);
        qusTxt = (TextView) findViewById(R.id.qus);
        ansTxt = (TextView) findViewById(R.id.ans);

        wrongWordQuestionArrayList.clear();
        wrongWordAnswerArrayList.clear();
        itemKeyValueArrayList.clear();

        Intent i = getIntent();
        deckValue = i.getStringExtra("deckValueKey");
        deckTitle = i.getStringExtra("deckValueListItem");

        questions = new ArrayList<>();
        answers = new ArrayList<>();

        questions.clear();
        answers.clear();
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/items/" + auth.getCurrentUser().getUid() + "/" + deckValue);
        final ChildEventListener childEventListener = mRootRef.orderByChild("active").equalTo("-1").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//               Toast.makeText(getApplicationContext(),"Item Key: "+dataSnapshot.getKey(),Toast.LENGTH_LONG).show();
                Map<String, String> map = dataSnapshot.getValue(Map.class);
                itemKeyValueArrayList.add(dataSnapshot.getKey());
                questions.add(map.get("cardA"));
                answers.add(map.get("cardB"));
                MyClickBtn(questions, answers);


            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        for(int j = 0;j<questions.size();j++){
            wrongWordQuestionArrayList.add(questions.get(j));
        }
//        Toast.makeText(getApplicationContext(),"Questions: "+wrongWordQuestionArrayList,Toast.LENGTH_LONG).show();
//        MyClickBtn(questions, answers);
    }

    public void MyClickBtn(final ArrayList<String> questions, final ArrayList<String> answers){

        mp = MediaPlayer.create(this, R.raw.hyu);
        mp1 = MediaPlayer.create(this,R.raw.button);
        qusTxt.setText(questions.get(counter));
        ansTxt.setText("");

        ansTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "Array Size:  " + questions.size(), Toast.LENGTH_LONG).show();
                if(counter>questions.size()-1){
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(WrongWordsTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    startActivity(intent);
                    finish();

                }
                else {
                    ansTxt.setText(answers.get(counter));

//                        Toast.makeText(getApplicationContext(), "Counter: " + counter, Toast.LENGTH_LONG).show();
                }
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                counter++;
                happyCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/"+auth.getCurrentUser().getUid()+"/"+deckValue+"/"+itemKeyValueArrayList.get(counter-1));
                childRefItem.child("active").setValue("1");
//                            updateData(deckValue, "1");
                if(counter>questions.size()-1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    retryBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(WrongWordsTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    startActivity(intent);
                    finish();
                }
                else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
//                    Toast.makeText(getApplicationContext(), "Happy: " + happyCount, Toast.LENGTH_LONG).show();
                }
            }

        });

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp1.start();
                counter++;
                sadCount++;
                childRefItem = new Firebase("https://tangoo2-54486.firebaseio.com/items/"+auth.getCurrentUser().getUid()+"/"+deckValue+"/"+itemKeyValueArrayList.get(counter-1));
                childRefItem.child("active").setValue("-1");
                if(counter>questions.size()-1) {
//            Toast.makeText(getApplicationContext(),"Khel Khotom",Toast.LENGTH_LONG).show();
                    okBtn.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(WrongWordsTestActivity.this, ItemsActivity.class);
                    intent.putExtra("happyCount", happyCount);
                    intent.putExtra("deckValueKey", deckValue);
                    intent.putExtra("sadCount", sadCount);
                    intent.putExtra("deckTitle", deckTitle);
                    startActivity(intent);
                    finish();
                }
                else {
                    qusTxt.setText(questions.get(counter));
                    ansTxt.setText("");
//                        Toast.makeText(getApplicationContext(), "Sad: " + sadCount, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    
}
