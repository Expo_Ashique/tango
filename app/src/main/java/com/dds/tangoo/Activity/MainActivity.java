package com.dds.tangoo.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.tangoo.Model.Deck;
import com.dds.tangoo.R;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class MainActivity extends Activity implements View.OnClickListener {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    Button create_deck_btn;
    ListView deck_list;
    Context context = this;
    private DatabaseReference mDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private String userID, deckId;
    EditText deckInput;
    Button listAddingBtn;
    ImageView closeImageView;
    private Firebase mRootRef;
    ArrayList<String> deckList = new ArrayList<>();
    ArrayList<String> deckIdList = new ArrayList<>();
    ArrayAdapter<String> deckAdapter;
    FirebaseListAdapter<Deck> firebaseListAdapter;
    Firebase childRefDeck, childRefItem;
    ProgressDialog progressDialog;
    String deckListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deck_list = (ListView) findViewById(R.id.deck_list);
        create_deck_btn = (Button) findViewById(R.id.create_deck_btn);

        auth = FirebaseAuth.getInstance();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tangoo2-54486.firebaseio.com/decks/" + auth.getCurrentUser().getUid());
        databaseReference.keepSynced(true);
        firebaseListAdapter = new FirebaseListAdapter<Deck>(this, Deck.class,
                R.layout.new_list, databaseReference) {

            @Override
            protected void populateView(View v, Deck model, int position) {
                TextView textView = (TextView) v.findViewById(R.id.list_text);
                textView.setText(model.getTitle());

            }

        };

        deck_list.setAdapter(firebaseListAdapter);
        create_deck_btn.setOnClickListener(this);
        deck_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Deck deck = (Deck) parent.getItemAtPosition(position);
                deckListItem = deck.getTitle();
                Toast.makeText(getApplicationContext(), "Please wait ...", Toast.LENGTH_SHORT).show();

                Firebase mRoot = new Firebase("https://tangoo2-54486.firebaseio.com/decks");
                deckList.clear();
                String userid = auth.getCurrentUser().getUid();
                Firebase childRef = mRoot.child(userid);

                final com.firebase.client.ValueEventListener valueEventListener = childRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
                    @Override
                    public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                        for (com.firebase.client.DataSnapshot deckSnapShot : dataSnapshot.getChildren()) {
                            String deckIdValue = deckSnapShot.getKey();
                            deckIdList.add(deckIdValue);
                        }

                        for (int i = 0; i < deckIdList.size(); i++) {
                            if (position == i) {
                                String decks = deckIdList.get(i);
                                Log.v("Keys", "" + decks);
                                Intent intent = new Intent(MainActivity.this, ItemsActivity.class);
                                intent.putExtra("deckValueKey", decks);
                                intent.putExtra("deckTitle", deckListItem);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


            }
        });
    }

    public void signOut() {
        auth.signOut();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.add_deck, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        alertDialogBuilder.setView(promptsView);
        deckInput = (EditText) promptsView.findViewById(R.id.list_input_field);
        closeImageView = (ImageView) promptsView.findViewById(R.id.close_image_view);
        listAddingBtn = (Button) promptsView.findViewById(R.id.ok_btn);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        listAddingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewDeck();
                alertDialog.dismiss();
            }
        });

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void createNewDeck() {
        mRootRef = new Firebase("https://tangoo2-54486.firebaseio.com/");
        childRefDeck = mRootRef.child("decks");
        final String titleName = deckInput.getText().toString();
        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        userID = auth.getCurrentUser().getUid();
        deckId = mRootRef.push().getKey();

        Deck deck = new Deck(titleName, currentDateTimeString);
        deck.setTitle(titleName);
        deck.setTimeStamp(currentDateTimeString);

        //
        childRefDeck.child(userID).child(deckId).setValue(deck);
        Toast.makeText(getApplicationContext(), "Uploading ...", Toast.LENGTH_LONG).show();
    }

    //this is crush works
    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }


    private void getDeckData() {
        Firebase mRoot = new Firebase("https://tangoo2-54486.firebaseio.com/decks");
        deckList.clear();
        String userid = auth.getCurrentUser().getUid();
        Firebase childRef = mRoot.child(userid);

        final com.firebase.client.ValueEventListener valueEventListener = childRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                for (com.firebase.client.DataSnapshot deckSnapShot : dataSnapshot.getChildren()) {
                    //String value = dataSnapshot.getValue(String.class);
                    Map<String, String> map = deckSnapShot.getValue(Map.class);
                    String title = map.get("title");
                    Log.v("Values", "Item: " + title);
                    deckList.add(title);

                    deckAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


}
